import pandas as pd
import sys

local_path = '/cmsnfshome0/nfshome0/alshevel/bril/x86_64_centos7/lib/libbrilhfsource.so'
mhtr_table = 'file:///opt/xdaq/ipbus/hcal/uHTR.xml'
luts_path = '/opt/xdaq/htdocs/bril/hardwareconfig/xml/luts_hf2022_v0.txt'

hfstore_path = '/localdata/alshevel/hflumi'

crates = [22, 29, 32]
boards = [x for x in range(1,13,1)]
fibers = [x for x in range(24)]
channels = [x for x in range(4)]
#etas = [29, 30, 31, 32, 33, 34]
etas = [31, 32]

name = 'hf_bbmd_'

# Check if we need to generate config local version or production one
local = True
add_local_lumistore = True

class Mask:
    def __init__(self, crate, board, fiber):
        self.crate = crate
        self.board = board
        self.fiber = fiber
        self.mask_list = [1, 1, 1, 1]
        self.mask_value = 15

    def unmask_ch(self, ch_number):
        self.mask_list[ch_number] = 0

    def convert(self):
        num = 0
        for i in reversed(self.mask_list):
            num = 2 * num + i
        self.mask_value = num

def generate_header():

    if local:
        print('<?xml version="1.0" encoding="UTF-8"?>', file = f)
        print('<xc:Partition xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', file = f)
        print('', file = f)
        print('<xc:Context url="http://srv-s2d16-29-01.cms:42000">', file = f)
        print('<xc:Endpoint protocol="utcp" service="b2in" rmode="select" hostname="srv-s2d16-29-01.cms" port="42001" network="utcp1" sndTimeout="2000" rcvTimeout="2000" affinity="RCV:P,SND:W,DSR:W,DSS:W" singleThread="true" publish="true"/>', file = f)
        print('', file = f)
        print('<xc:Application class="eventing::core::Publisher" id="101" instance="0" network="utcp1" group="b2in" service="eventing-publisher" bus="hf" logpolicy="inherit">', file = f)
        print('<properties xmlns="urn:xdaq-application:eventing::core::Publisher" xsi:type="soapenc:Struct">', file = f)
        print('<eventings xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[2]">', file = f)
        print('<item xsi:type="xsd:string" soapenc:position="[0]">utcp://srv-s2d16-14-01.cms:8185</item>', file = f)
        print('<item xsi:type="xsd:string" soapenc:position="[1]">utcp://srv-s2d16-27-01.cms:8185</item>', file = f)
        print('</eventings>', file = f)
        print('</properties>', file = f)
        print('</xc:Application>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xc:Module>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xc:Module>', file = f)
        print('', file = f)
        
        print('<xc:Application class="eventing::core::Subscriber" id="102" instance="0" network="utcp1" group="b2in" service="eventing-subscriber" bus="brildata" logpolicy="inherit">', file = f)
        print('<properties xmlns="urn:xdaq-application:eventing::core::Subscriber" xsi:type="soapenc:Struct">', file = f)
        print('<eventings xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[2]">', file = f)
        print('<item xsi:type="xsd:string" soapenc:position="[0]">utcp://srv-s2d16-14-01.cms:8185</item>', file = f)
        print('<item xsi:type="xsd:string" soapenc:position="[1]">utcp://srv-s2d16-27-01.cms:8185</item>', file = f)
        print('</eventings>', file = f)
        print('</properties>', file = f)
        print('</xc:Application>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xc:Module>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xc:Module>', file = f)
        print('', file = f)

    else:
        print('<?xml version="1.0" encoding="UTF-8"?>', file = f)
        print('<xc:Partition xmlns:ns1="urn:xdaq-application:eventing::core::Publisher" xmlns:ns4="urn:xdaq-application:bril::hfsource::Application" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">', file = f)
        print('', file = f)
        print('<xc:Context url="http://srv-s2d16-14-01.cms:6210">', file = f)
        print('<xc:Endpoint affinity="RCV:P,SND:W,DSR:W,DSS:W" hostname="srv-s2d16-14-01.cms" network="utcp1" port="6211" protocol="utcp" publish="true" rcvTimeout="2000" rmode="select" service="b2in" singleThread="true" sndTimeout="2000" />', file = f)
        print('', file = f)
        print('<xc:Application bus="hf" class="eventing::core::Publisher" group="b2in" id="101" instance="0" logpolicy="inherit" network="utcp1" service="eventing-publisher">', file = f)
        print('<ns1:properties xsi:type="soapenc:Struct">', file = f)
        print('<ns1:eventings soapenc:arrayType="xsd:ur-type[2]" xsi:type="soapenc:Array">', file = f)
        print('<ns1:item soapenc:position="[0]" xsi:type="xsd:string">utcp://srv-s2d16-14-01.cms:8185</ns1:item>', file = f)
        print('<ns1:item soapenc:position="[1]" xsi:type="xsd:string">utcp://srv-s2d16-27-01.cms:8185</ns1:item>', file = f)
        print('</ns1:eventings>', file = f)
        print('</ns1:properties>', file = f)
        print('</xc:Application>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xc:Module>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xc:Module>', file = f)

def generate_tail():
    if local:
        print('<xc:Application class="xmem::probe::Application" id="21" instance="0" network="local" >', file = f)
        print('</xc:Application>', file = f)
        print('<xc:Module>${XDAQ_ROOT}/lib/libxmemprobe.so</xc:Module>', file = f)

    else:
        pass
        
def generate_close():
    print('</xc:Context>', file = f)
    print('</xc:Partition>', file = f)
    print('', file = f)

def generate_production_v(crate, board, counter, mask_print):
    print('<xc:Application class="bril::hfsource::Application" id="{}{}" instance="{}" logpolicy="inherit" network="local" service="hfsource">'.format(board, crate, counter), file = f)
    print('<ns4:properties xsi:type="soapenc:Struct">', file = f)
    print('<ns4:addressTable xsi:type="xsd:string">{}</ns4:addressTable>'.format(mhtr_table), file = f)
    print('<ns4:luts_path xsi:type="xsd:string">{}</ns4:luts_path>'.format(luts_path), file = f)
    print('<ns4:uhtrAddress xsi:type="xsd:string">chtcp-2.0://bridge-s2f07-27:10203?target=hcal-uhtr-{}-{}:50001</ns4:uhtrAddress>'.format(crate, str(board).zfill(2)), file = f)
    print('<ns4:bus xsi:type="xsd:string">hf</ns4:bus>', file = f)
    print('<ns4:topics xsi:type="xsd:string">hfCMS1,hfCMS_ET,hfCMS_VALID</ns4:topics>', file = f)
    print('<ns4:histsToRead xsi:type="xsd:string">CMS1,CMS_ET,CMS_VALID</ns4:histsToRead>', file = f)
    print('<ns4:orbits_per_nibble xsi:type="xsd:integer">4096</ns4:orbits_per_nibble>', file = f)
    print('<ns4:integration_period_nb xsi:type="xsd:integer">1</ns4:integration_period_nb>', file = f)
    print('<ns4:nibbles_per_section xsi:type="xsd:integer">64</ns4:nibbles_per_section>', file = f)
    print('<ns4:cms1_threshold xsi:type="xsd:integer">8</ns4:cms1_threshold>', file = f)
    print('<ns4:cms2_threshold xsi:type="xsd:integer">8</ns4:cms2_threshold>', file = f)
    print('<ns4:orbit_phase xsi:type="xsd:integer">3530</ns4:orbit_phase>', file = f)
    print('<ns4:BXOffset xsi:type="xsd:integer">0</ns4:BXOffset>', file = f)
    print('<ns4:channels_to_mask xsi:type="xsd:string">{}</ns4:channels_to_mask>'.format(mask_print), file = f)
    print('</ns4:properties>', file = f)
    print('</xc:Application>', file = f)
    print('<xc:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xc:Module>', file = f)
    print('<xc:Module>${XDAQ_ROOT}/lib/libbrilhfsource.so</xc:Module>', file = f)
    print('', file = f)

def generate_local_v(crate, board, counter, mask_print):
    print('<xc:Application class="bril::hfsource::Application" id="{}{}" instance="{}" logpolicy="inherit" network="local" service="hfsource">'.format(board, crate, counter), file = f)
    print('<properties xmlns="urn:xdaq-application:bril::hfsource::Application" xsi:type="soapenc:Struct">', file = f)
    print('<bus xsi:type="xsd:string">hf</bus>', file = f)
    print('<addressTable xsi:type="xsd:string">{}</addressTable>'.format(mhtr_table), file = f)
    print('<luts_path xsi:type="xsd:string">{}</luts_path>'.format(luts_path), file = f)
    print('<uhtrAddress xsi:type="xsd:string">chtcp-2.0://bridge-s2f07-27:10203?target=hcal-uhtr-{}-{}:50001</uhtrAddress>'.format(crate, str(board).zfill(2)), file = f)
    print('<topics xsi:type="xsd:string">hfCMS1,hfCMS_ET,hfCMS_VALID</topics>', file = f)
    print('<histsToRead xsi:type="xsd:string">CMS1,CMS_ET,CMS_VALID</histsToRead>', file = f)
    print('<orbits_per_nibble xsi:type="xsd:integer">4096</orbits_per_nibble>', file = f)
    print('<integration_period_nb xsi:type="xsd:integer">1</integration_period_nb>', file = f)
    print('<nibbles_per_section xsi:type="xsd:integer">64</nibbles_per_section>', file = f)
    print('<cms1_threshold xsi:type="xsd:integer">8</cms1_threshold>', file = f)
    print('<cms2_threshold xsi:type="xsd:integer">8</cms2_threshold>', file = f)
    print('<orbit_phase xsi:type="xsd:integer">3530</orbit_phase>', file = f)
    print('<BXOffset xsi:type="xsd:integer">0</BXOffset>', file = f)
    print('<channels_to_mask xsi:type="xsd:string">{}</channels_to_mask>'.format(mask_print), file = f)
    print('</properties>', file = f)
    print('</xc:Application>', file = f)
    print('<xc:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xc:Module>', file = f)
    print('<xc:Module>{}</xc:Module>'.format(local_path), file = f)
    print('', file = f)

def generate_lumistore():

    print('<xc:Application class="bril::lumistore::Application" id="500" instance="1" logpolicy="inherit" network="local" service="lumistore">', file = f)
    print('<properties xsi:type="soapenc:Struct">', file = f)
    print('<datasources soapenc:arrayType="xsd:ur-type[1]" xsi:type="soapenc:Array">', file = f)
    print('<item soapenc:position="[0]" xsi:type="soapenc:Struct">', file = f)
    print('<properties xsi:type="soapenc:Struct">', file = f)
    print('<bus xsi:type="xsd:string">hf</bus>', file = f)
    print('<topics xsi:type="xsd:string">hfCMS1,hfCMS_ET,hfCMS_VALID</topics>', file = f)
    print('</properties>', file = f)
    print('</item>', file = f)
    print('</datasources>', file = f)
    print('<maxstalesec xsi:type="xsd:unsignedInt">30</maxstalesec>', file = f)
    print('<checkagesec xsi:type="xsd:unsignedInt">10</checkagesec>', file = f)
    print('<maxsizeMB xsi:type="xsd:unsignedInt">100</maxsizeMB>', file = f)
    print('<fileformat xsi:type="xsd:string">hd5</fileformat>', file = f)
    print('<nrowperwbuf xsi:type="xsd:unsignedInt">12</nrowperwbuf>', file = f)
    print('<filepath xsi:type="xsd:string">{}</filepath>'.format(hfstore_path), file = f)
    print('</properties>', file = f)
    print('</xc:Application>', file = f)
    print('<xc:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xc:Module>', file = f)
    print('<xc:Module>${XDAQ_ROOT}/lib/libbrillumistore.so</xc:Module>', file = f)
    print('', file = f)
    
def generate_eta_rings(local = True):
    counter = 0
    for crate in crates:
        for board in boards:
            masks_per_board = []
            for fiber in fibers:
                mask = Mask(crate, board, fiber)
                for ch in channels:
                    df_new = df.loc[(df['Crate'] == crate) & (df['uHTR'] == board) & (df['uHTR_FI'] == fiber) & (df['FI_CH'] == ch)]
                    if df_new.empty:
                        pass
                    else:
                        mask.unmask_ch(ch)

                mask.convert()
                masks_per_board.append(mask.mask_value)

            counter = counter + 1

            sarr = [str(a) for a in masks_per_board]
            mask_print = ", ".join(sarr)
            mask_print = mask_print.replace(" ", "")

            if local:
                generate_local_v(crate, board, counter, mask_print)
            else:
                generate_production_v(crate, board, counter, mask_print)

def generate_custom(mask):
    counter = 0
    for crate in crates:
        for board in boards:
            counter += 1
            generate_local_v(crate, board, counter, mask)

df = pd.read_csv(sys.argv[1])

# Drop if not HF
df.drop(df[df['Det'] != 'HF'].index, inplace = True)
print(df)

drop_etas = True
if drop_etas:
    # First drop the wrong etas
    df.drop(df[~df.Eta.isin(etas)].index, inplace=True)
    
# Provide here your custom mask
masks = [ [11, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [7, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 13, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 11, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 7, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 13, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 11, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 7, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 13, 15, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 11, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 7, 15, 15, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 14, 15, 15, 15, 15, 15, 15]
        , [15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 13, 15, 15, 15, 15, 15, 15]]

mask_number = 0
for mask in masks:
    f = open(f'configs/test_config_{mask_number}.xml', mode='w')
    
    sarr = [str(a) for a in mask]
    mask_print = ", ".join(sarr)
    mask_print = mask_print.replace(" ", "")
    mask_print = mask_print.replace("[", "")
    mask_print = mask_print.replace("]", "")

    
    generate_header()
    generate_custom(mask_print)
    generate_tail()
    #generate_lumistore()
    generate_close()
    mask_number += 1
